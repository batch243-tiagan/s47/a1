// console.log("Congrats, Batch243");

const txtFirstName = document.querySelector("#txt-first-name");
// console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");

const name = document.querySelectorAll(".full-name");
// console.log(name);

const span = document.querySelectorAll("span");
// console.log(span);

const text = document.querySelectorAll("input[type]");
// console.log(text);

const spanFullName = document.querySelector("#fullName");

const colorSelecter = document.querySelector("#selectColor");

const colorPicker = document.querySelector("#color-picker");

// txtFirstName.addEventListener("keyup", (event) =>{
// 	console.log(event.target.value);
// });

const fullName = () => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

colorPicker.addEventListener("change", (event) =>{
	spanFullName.style.color = event.target.value;
	colorSelecter.value = "#000000";
});

colorSelecter.addEventListener("change", (event) =>{
	spanFullName.style.color = event.target.value;
	colorPicker.value =  event.target.value;
});